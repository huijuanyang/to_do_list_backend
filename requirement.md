# 前置知识说明
要完成本应用程序，需要您具备以下知识, 如果您对下面的知识已经熟悉，可以不必学习。你可以把下面的资料当做知识手册来查询。

## 必学知识：
##### 1. [Building an Application with Spring Boot](https://spring.io/guides/gs/spring-boot/)
##### 2. [Spring Boot Annotations](https://www.baeldung.com/spring-boot-annotations)
##### 3. [Spring Boot @Controller](http://zetcode.com/springboot/controller/)
##### 4. [Learn REST: A RESTful Tutorial](https://www.restapitutorial.com/)
##### 5. [Get Started with Postman](https://learning.postman.com/getting-started/)
##### 6. [Java Database Connectivity with MySQL](Java Database Connectivity with MySQL)
